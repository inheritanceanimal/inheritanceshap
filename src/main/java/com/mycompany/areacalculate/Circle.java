/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.areacalculate;

/**
 *
 * @author nymr3kt
 */
public class Circle extends Shap{
    private double r ;
    public static final double pi = 22.0 / 7 ;
    
    public Circle(String name , double r ){
        super(name);
        this.name = name ;
        this.r = r ;
        
    }
    
    public double CalArea(){
        return pi * r * r ;
        
    }
    public double getR(double r){
        return r;
    }
    public void setR(double r){
        if(r<=0){
            System.out.println("Error : Radius must more than zero");
            return ;
        }
        this.r = r;
    }
}
