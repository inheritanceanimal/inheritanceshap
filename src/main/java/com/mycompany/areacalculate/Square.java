/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.areacalculate;

/**
 *
 * @author nymr3kt
 */
public class Square extends Ractangle {

    protected double w;
    protected double h ;

    public Square(String name, double w) {
        super(w, w, name);
        this.h = w;
        this.w = w;
        this.name = name;

    }
    @Override
    public double CalArea(){
        return w * w ;

    }
    public void setW(double w){
        if(w <= 0){
            System.out.println("Error: Wide must more than Zero!!");
            return ;
}
        this.w = w;
    }
    @Override
    public double getW(double w){
        return w ;
    }
}

