/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.areacalculate;

/**
 *
 * @author nymr3kt
 */
public class Ractangle {
    protected double h;
    protected double w;
    protected String name ;

    public Ractangle(double h, double w, String name) {
        this.h = h;
        this.w = w;
        this.name = name ;
    }

    public double CalArea(){
        return h * w;

    }

    public void setHandW(double h, double w) {
        if (h <= 0 && w <= 0) {
            System.out.println("Error: Hight and Wide must more than zero!!");
            return;
        }
        this.h = h;
        this.w = w;
    }

    public void nameShape() {
        System.out.println("            Create Shape");
        System.out.println("My name is : " + name);

    }

    public void end() {
        System.out.println("-----------------------------------------------");
    }

    public double getH(double h) {
        return h;

    }

    public double getW(double w) {
        return w;

    }

}
